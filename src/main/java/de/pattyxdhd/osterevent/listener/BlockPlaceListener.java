package de.pattyxdhd.osterevent.listener;

import de.pattyxdhd.osterevent.OsterEvent;
import de.pattyxdhd.osterevent.utils.itemcache.ItemCache;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.TileEntitySkull;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

    // TODO: remove "v1_8_R3" import

    @EventHandler
    public void onPlace(final BlockPlaceEvent event){

        if(event.getItemInHand().getItemMeta().getDisplayName() == null){
            return;
        }

        if(event.getItemInHand().getItemMeta().getDisplayName().equals(ItemCache.getPlaceEgg().getItemMeta().getDisplayName())){

            if(!OsterEvent.getInstance().getEggManager().canBuild(event.getPlayer())){
                event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§cDu musst dich im BuildMode befinden.");
                event.setCancelled(true);
                return;
            }

            if(!OsterEvent.getInstance().getEggManager().exists(event.getBlockPlaced().getLocation())){
                OsterEvent.getInstance().getEggManager().addEgg(event.getBlockPlaced().getLocation());

                final Block block = event.getBlock();
                final Block blockAgainst = event.getBlockAgainst();

                final int facing;

                if(block.getRelative(BlockFace.NORTH).getLocation().distance(blockAgainst.getLocation()) == 0.0D) {
                    facing = 3;
                } else if(block.getRelative(BlockFace.SOUTH).getLocation().distance(blockAgainst.getLocation()) == 0.0D) {
                    facing = 2;
                } else if(block.getRelative(BlockFace.EAST).getLocation().distance(blockAgainst.getLocation()) == 0.0D) {
                    facing = 4;
                } else if(block.getRelative(BlockFace.WEST).getLocation().distance(blockAgainst.getLocation()) == 0.0D) {
                    facing = 5;
                } else {
                    facing = 1;
                }

                block.setType(Material.SKULL);
                block.setData((byte)facing);

                Skull skullData = (Skull)block.getState();
                skullData.setSkullType(SkullType.PLAYER);

                TileEntitySkull skullTile = (TileEntitySkull)((CraftWorld)block.getWorld()).getHandle().getTileEntity(new BlockPosition(block.getX(), block.getY(), block.getZ()));
                skullTile.setGameProfile(ItemCache.getRandomEgg());
                block.getState().update(true);

                event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§aDu hast erfolgreich ein Ei gesetzt. §8(§b" + OsterEvent.getInstance().getEggManager().getEggCount() + " Eier§8)");
                if(OsterEvent.getInstance().getFileManager().isUseSounds()) event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 1, 2);
            }else{
                event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§cAn dieser stelle befindet sich schon ein Ei.");
                event.setCancelled(true);
            }
        }


    }

}
