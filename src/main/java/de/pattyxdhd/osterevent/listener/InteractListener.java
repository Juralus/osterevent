package de.pattyxdhd.osterevent.listener;

import de.pattyxdhd.osterevent.OsterEvent;
import de.pattyxdhd.osterevent.utils.file.FileManager;
import de.pattyxdhd.osterevent.utils.firework.FireworkUtil;
import de.pattyxdhd.osterevent.utils.objects.EasterPlayer;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class InteractListener implements Listener {

    @Getter
    private FileManager fileManager;

    public InteractListener(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event){
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            if(event.getClickedBlock().getState() instanceof Skull){
                if(OsterEvent.getInstance().getEggManager().canBuild(event.getPlayer())){
                    return;
                }

                final Location eggLocation = event.getClickedBlock().getLocation();
                final Player player = event.getPlayer();

                if(OsterEvent.getInstance().getEggManager().exists(eggLocation)){
                    final EasterPlayer easterPlayer = OsterEvent.getInstance().getDatabaseHandler().getPlayers().get(player.getUniqueId().toString());

                    if(easterPlayer == null){
                        event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§4Fehler: §cBitte verbinde dich erneut zum Server.");
                        OsterEvent.getInstance().log("§4Fehler: §cDer Spieler §e" + event.getPlayer().getName() + " §cwurde im Cache nicht gefunden.");
                        return;
                    }


                    if(!OsterEvent.getInstance().getEggManager().isFound(easterPlayer, eggLocation)){
                        OsterEvent.getInstance().getEggManager().markAsFound(easterPlayer, eggLocation);

                        if(easterPlayer.getFoundCounter() >= OsterEvent.getInstance().getEggManager().getEggCount()){
                            player.sendMessage(getFileManager().getAllEggsFound()
                                    .replace("%prefix%", getFileManager().getPrefix())
                                    .replace("%foundEggs%", easterPlayer.getFoundCounter().toString())
                                    .replace("%maxEggs%", OsterEvent.getInstance().getEggManager().getEggCount().toString()));

                            if(getFileManager().isUseSounds()) player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
                            if(getFileManager().isUseFireworks()) FireworkUtil.firework(player);

                            getFileManager().getAllEggsFoundCMDs().forEach(cmd -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%playerName%", player.getName())));

                            easterPlayer.addAndShowHologram(player, eggLocation);

                        }else{
                            player.sendMessage(getFileManager().getEggFound()
                                    .replace("%prefix%", getFileManager().getPrefix())
                                    .replace("%foundEggs%", easterPlayer.getFoundCounter().toString())
                                    .replace("%maxEggs%", OsterEvent.getInstance().getEggManager().getEggCount().toString()));
                            if(getFileManager().isUseSounds()) player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
                            getFileManager().getEggFoundCMDs().forEach(cmd -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%playerName%", player.getName())));

                            easterPlayer.addAndShowHologram(player, eggLocation);

                        }
                    }else{
                        if(getFileManager().isUseSounds()) player.playSound(player.getLocation(), Sound.BAT_DEATH, 1, 2);
                        if(easterPlayer.getFoundCounter() >= OsterEvent.getInstance().getEggManager().getEggCount()){
                            player.sendMessage(getFileManager().getAllEggsAlreadyFound()
                                    .replace("%prefix%", getFileManager().getPrefix())
                                    .replace("%foundEggs%", easterPlayer.getFoundCounter().toString())
                                    .replace("%maxEggs%", OsterEvent.getInstance().getEggManager().getEggCount().toString()));
                            return;
                        }

                        player.sendMessage(getFileManager().getEggAlreadyFound()
                                .replace("%prefix%", getFileManager().getPrefix()));
                    }
                }
            }
        }
    }


}
