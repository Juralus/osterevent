package de.pattyxdhd.osterevent.manager;

import com.google.common.collect.Lists;
import de.pattyxdhd.osterevent.OsterEvent;
import de.pattyxdhd.osterevent.utils.file.FileManager;
import de.pattyxdhd.osterevent.utils.objects.EasterPlayer;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class EggManager {

    @Getter
    private FileManager fileManager;

    @Getter(AccessLevel.PRIVATE)
    private List<UUID> buildMode = Lists.newArrayList();

    public EggManager(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    public void addEgg(final Location location){
        fileManager.getEggs().add(location);
        fileManager.saveEggs();
    }

    public void removeEgg(final Location location){
        fileManager.getEggs().remove(location);
        fileManager.saveEggs();
    }

    public Integer getEggCount(){
        if(fileManager.getEggs().isEmpty()){
            return 0;
        }
        return fileManager.getEggs().size();
    }

    public boolean exists(final Location location){
        return fileManager.getEggs().contains(location);
    }

    public void toggleBuild(final Player player){
        if(canBuild(player)){
            getBuildMode().remove(player.getUniqueId());
            player.sendMessage(getFileManager().getPrefix() + "§cDu bist nun nicht mehr im Buildmode.");
        }else{
            getBuildMode().add(player.getUniqueId());
            player.sendMessage(getFileManager().getPrefix() + "§eDu bist nun im BuildMode.");
        }
    }

    public boolean isFound(final EasterPlayer easterPlayer, final Location location){
        return easterPlayer.getFoundLocations().contains(location);
    }

    public void markAsFound(final EasterPlayer easterPlayer, final Location location){
        easterPlayer.getFoundLocations().add(location);
        OsterEvent.getInstance().getDatabaseHandler().updatePlayer(easterPlayer);
    }

    public boolean canBuild(final Player player){
        return getBuildMode().contains(player.getUniqueId());
    }

}
