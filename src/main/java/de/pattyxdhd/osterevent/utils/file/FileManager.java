package de.pattyxdhd.osterevent.utils.file;

import com.google.common.collect.Lists;
import de.pattyxdhd.osterevent.OsterEvent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

import java.util.List;

@Getter @Setter(AccessLevel.PRIVATE)
public class FileManager {

    private FileWriter fileWriter;

    private String host;
    private Integer port;
    private String database;
    private String user;
    private String password;

    private String prefix = "§8[§aOsterEvent§8] §7";
    private String noPerm = "%prefix%§cDazu hast du keinen Zugriff.";
    private String deleteKickMessage = "%prefix%§cDu wurdest zurückgesetzt.";

    private String eggFound = "%prefix%§aHerzlichen Glückwunsch, du hast ein weiteres Ei gefunden. §8(§b%foundEggs%§7/§9%maxEggs%§8)";
    private String eggAlreadyFound = "%prefix%§cDu hast dieses Ei bereits gefunden.";
    private String allEggsFound = "%prefix%§bHerzlichen Glückwunsch, du hast nun alle Eier gefunden. §8(§a%foundEggs%§7/§2%maxEggs%§8)";
    private String allEggsAlreadyFound = "%prefix%§eDu kannst aufhören zu suchen, du hast bereits alle Eier gefunden.";

    private List<String> eggFoundCMDs = Lists.newArrayList("tell %playerName% Du kannst die Commands in der config.yml ändern.");
    private List<String> allEggsFoundCMDs = Lists.newArrayList("tell %playerName% Du kannst die Commands in der config.yml ändern.");

    private boolean holoUse = true;
    private List<String> holoText = Lists.newArrayList("§8« §aGefunden §8»");

    private boolean useSounds = true;
    private boolean useFireworks = true;

    private List<Location> eggs = Lists.newArrayList();


    public FileManager(final OsterEvent osterEvent) {
        fileWriter = new FileWriter(osterEvent.getDataFolder().getPath(), "config.yml");

        loadFile();
        readFile();
    }

    private void loadFile(){
        fileWriter.setDefaultValue("MySQL.Host", "127.0.0.1");
        fileWriter.setDefaultValue("MySQL.Port", 3306);
        fileWriter.setDefaultValue("MySQL.Database", "YouTuberSystem");
        fileWriter.setDefaultValue("MySQL.User", "YouTuberSystem");
        fileWriter.setDefaultValue("MySQL.Password", "SicheresPassword");

        fileWriter.setDefaultValue("Message.Prefix", getPrefix().replace("§", "&"));
        fileWriter.setDefaultValue("Message.noPerm", getNoPerm().replace("§", "&"));
        fileWriter.setDefaultValue("Message.deleteKick", getDeleteKickMessage().replace("§", "&"));

        fileWriter.setDefaultValue("Message.eggFound", eggFound.replace("§", "&"));
        fileWriter.setDefaultValue("Message.eggAlreadyFound", eggAlreadyFound.replace("§", "&"));
        fileWriter.setDefaultValue("Message.allEggsFound", allEggsFound.replace("§", "&"));
        fileWriter.setDefaultValue("Message.allEggsAlreadyFound", allEggsAlreadyFound.replace("§", "&"));

        fileWriter.setDefaultValue("Hologram.Use", holoUse);
        fileWriter.setDefaultValue("Hologram.Text", OsterEvent.getInstance().getConverter().replace(holoText, "§", "&"));

        fileWriter.setDefaultValue("Sounds", useSounds);
        fileWriter.setDefaultValue("Firework", useFireworks);

        fileWriter.setDefaultValue("Commands.eggFound", eggFoundCMDs);
        fileWriter.setDefaultValue("Commands.allEggsFound", allEggsFoundCMDs);

        fileWriter.setDefaultValue("Eggs", OsterEvent.getInstance().getConverter().convertFromLocationListToStringlist(eggs));


        fileWriter.save();
    }

    private void readFile(){
        setHost(fileWriter.getString("MySQL.Host"));
        setPort(fileWriter.getInt("MySQL.Port"));
        setDatabase(fileWriter.getString("MySQL.Database"));
        setUser(fileWriter.getString("MySQL.User"));
        setPassword(fileWriter.getString("MySQL.Password"));

        setPrefix(fileWriter.getFormatString("Message.Prefix"));
        setNoPerm(fileWriter.getFormatString("Message.noPerm"));
        setDeleteKickMessage(fileWriter.getFormatString("Message.deleteKick"));

        setEggFound(fileWriter.getFormatString("Message.eggFound"));
        setEggAlreadyFound(fileWriter.getFormatString("Message.eggAlreadyFound"));
        setAllEggsFound(fileWriter.getFormatString("Message.allEggsFound"));
        setAllEggsAlreadyFound(fileWriter.getFormatString("Message.allEggsAlreadyFound"));

        setHoloUse(fileWriter.getBoolean("Hologram.Use"));
        setHoloText(OsterEvent.getInstance().getConverter().translateColorCodes(fileWriter.getStringList("Hologram.Text")));

        setUseSounds(fileWriter.getBoolean("Sounds"));
        setUseFireworks(fileWriter.getBoolean("Firework"));

        setEggFoundCMDs(fileWriter.getStringList("Commands.eggFound"));
        setAllEggsFoundCMDs(fileWriter.getStringList("Commands.allEggsFound"));

        setEggs(OsterEvent.getInstance().getConverter().convertFromStringListToLocationlist(fileWriter.getStringList("Eggs")));
    }

    public void saveEggs(){
        fileWriter.setValue("Eggs", OsterEvent.getInstance().getConverter().convertFromLocationListToStringlist(eggs));
        fileWriter.save();
    }

    public void reloadFile(final OsterEvent osterEvent){
        fileWriter = new FileWriter(osterEvent.getDataFolder().getPath(), "config.yml");
        readFile();
        new Thread(() -> {
            OsterEvent.getInstance().getDatabaseHandler().getPlayers().forEach((s, easterPlayer) -> {
                easterPlayer.hideHolograms();
                easterPlayer.getHolograms().clear();
                easterPlayer.loadHolograms();
                easterPlayer.showHolograms();
            });
            Thread.currentThread().stop();
        }).start();
    }
}
