package de.pattyxdhd.osterevent.utils.firework;

import de.pattyxdhd.osterevent.OsterEvent;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkUtil {

    public static void firework(final Player player){

        Firework firework = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
        FireworkMeta fireworkMeta = firework.getFireworkMeta();

        FireworkEffect fireworkEffect = FireworkEffect.builder()
                .flicker(true)
                .withColor(Color.GREEN)
                .withColor(Color.AQUA)
                .withColor(Color.RED)
                .with(FireworkEffect.Type.BALL_LARGE)
                .trail(true)
                .build();

        fireworkMeta.addEffect(fireworkEffect);
        fireworkMeta.setPower(2);
        firework.setFireworkMeta(fireworkMeta);
        Bukkit.getScheduler().runTaskTimerAsynchronously(OsterEvent.getInstance(), firework::detonate, 5, 1);
    }

}
