package de.pattyxdhd.osterevent.utils.itembuilder;

import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CustomSkullBuilder {

    private Material material;
    private int amount;
    private short durability;
    private boolean unbreakable;
    private String displayName;
    private List<String> lore;
    private Map<Enchantment, Integer> enchantmentIntegerMap;
    private String url;

    public CustomSkullBuilder() {
        this.material = Material.SKULL_ITEM;
        this.amount = 1;
        this.durability = 3;
        this.unbreakable = false;
        this.enchantmentIntegerMap = Maps.newConcurrentMap();
    }

    public ItemStack build() {
        ItemStack itemStack = new ItemStack(this.material, this.amount, this.durability);
        ItemMeta skullMeta = itemStack.getItemMeta();
        if (this.unbreakable) {
            skullMeta.spigot().setUnbreakable(true);
        }

        if (this.displayName != null) {
            skullMeta.setDisplayName(this.displayName);
        }

        if (this.lore != null) {
            skullMeta.setLore(this.lore);
        }

        if (this.url != null) {
            GameProfile profile = new GameProfile(UUID.randomUUID(), null);
            profile.getProperties().put("textures", new Property("textures", url));
            try {
                Field profileField = skullMeta.getClass().getDeclaredField("profile");
                profileField.setAccessible(true);
                profileField.set(skullMeta, profile);

            } catch (IllegalArgumentException | NoSuchFieldException | SecurityException | IllegalAccessException error) {
                error.printStackTrace();
            }
        }

        itemStack.setItemMeta(skullMeta);
        this.enchantmentIntegerMap.forEach(itemStack::addUnsafeEnchantment);
        return itemStack;
    }

    public CustomSkullBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public CustomSkullBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public CustomSkullBuilder setDurability(short durability) {
        this.durability = durability;
        return this;
    }

    public CustomSkullBuilder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public CustomSkullBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public CustomSkullBuilder addEnchantment(Enchantment enchantment, int level) {
        this.enchantmentIntegerMap.put(enchantment, level);
        return this;
    }

    public CustomSkullBuilder setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public CustomSkullBuilder setSkullURL(String url) {
        this.url = url;
        return this;
    }


}
