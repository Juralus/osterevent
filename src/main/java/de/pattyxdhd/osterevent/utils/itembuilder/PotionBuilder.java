package de.pattyxdhd.osterevent.utils.itembuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PotionBuilder {

    private Potion potion;
    private Material material;
    private int amount;
    private short durability;
    private boolean unbreakable;
    private String displayName;
    private List<String> lore;
    private Map<PotionType, Integer> potionIntegerMap;
    private ArrayList<ItemFlag> itemFlags;

    public PotionBuilder() {
        this.material = Material.POTION;
        this.amount = 1;
        this.durability = 0;
        this.unbreakable = false;
        this.potionIntegerMap = Maps.newConcurrentMap();
        this.itemFlags = Lists.newArrayList();
    }

    public ItemStack build() {
        ItemStack itemStack = new ItemStack(this.material, this.amount, this.durability);
        PotionMeta itemMeta = (PotionMeta) itemStack.getItemMeta();
        if (this.unbreakable) {
            itemMeta.spigot().setUnbreakable(true);
        }

        if (this.displayName != null) {
            itemMeta.setDisplayName(this.displayName);
        }

        if (this.lore != null) {
            itemMeta.setLore(this.lore);
        }

        if(this.itemFlags != null){
            itemFlags.forEach(itemMeta::addItemFlags);
        }

        if(potion != null){
            potion.apply(itemStack);
        }

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public PotionBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public PotionBuilder setDurability(short durability) {
        this.durability = durability;
        return this;
    }

    public PotionBuilder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public PotionBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public PotionBuilder addItemFlag(ItemFlag itemFlag){
        this.itemFlags.add(itemFlag);
        return this;
    }

    public PotionBuilder setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public PotionBuilder setPotionType(PotionType potionType, int level){
        potion = new Potion(potionType, level);
        return this;
    }

    public PotionBuilder setSplash(boolean splash){
        if(potion != null){
            potion.setSplash(splash);
        }
        return this;
    }

    public PotionBuilder setExtendDuration(boolean duration){
        if(potion != null){
            potion.setHasExtendedDuration(duration);
        }
        return this;
    }

}
