package de.pattyxdhd.osterevent.utils.itemcache;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.pattyxdhd.osterevent.utils.itembuilder.ItemBuilder;
import de.pattyxdhd.osterevent.utils.math.MathUtil;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

@Getter
public enum ItemCache {

    PLACEEGG("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNjU2ZjdmM2YzNTM2NTA2NjI2ZDVmMzViNDVkN2ZkZjJkOGFhYjI2MDA4NDU2NjU5ZWZlYjkxZTRjM2E5YzUifX19"),
    PURPLEEGG("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmQ2YjQ2OWY2NmE1NzQ1OTZmMWViNTFmYWNkY2FhMTUyMmE0ODYwMTE3MDA4NTRjZWRjNDc5NmU0NGYyM2I5MCJ9fX0="),
    GREENEGG("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODZmYWFkN2U5OThjNWYzYjgzMzJjYTU1YzkwMTEyMGI5YWUxN2YwMTkzZGQ4ZTAxYTgzY2ZiOTU4MDg0ODMyNSJ9fX0="),
    BLUEEGG("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODI0ZmNkMjMxNDIxN2NkNWRhMTQ5ZDA2M2E0ZTcxNTliYzcwZGFlYTRhNjM2YWQ5MjNhNTRiMmFlMjQwZWFiMCJ9fX0="),
    REDEGG("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTI2YmE5Mzk0YTVmOGRkZWU4ZWUxZjJjMjQ1YzU3ZjU5ZWEwY2I2NDA4ODQ3NGIyNjA4MjA1ZmU4MjA1Yjc0NCJ9fX0="),
    YELLOWEGG("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWI0YWZiMDExMzUzYWZjNDUyNzI5NWNiMjMxNzhlMTViZTExM2I0ZWJhMTU5OGM0MGExNzRmZWVlZTc3OWYwNiJ9fX0=");

    private String url;

    ItemCache(String url) {
        this.url = url;
    }

    public static ItemStack getPlaceEgg(){
        return ItemBuilder.customSkullBuilder()
                .setSkullURL(PLACEEGG.getUrl())
                .setDisplayName("§aOsterei")
                .setLore(Lists.newArrayList("", "§7Mit diesem Ei kannst du", "§7die eier in der Lobby verteilen.", ""))
                .build();
    }

    public static GameProfile getRandomEgg() {
        List<String> differentEggs = Lists.newArrayList();
        for (ItemCache value : values()) {
            differentEggs.add(value.getUrl());
        }
        differentEggs.remove(0);
        String texture = differentEggs.get(MathUtil.randomInt(0, differentEggs.size()-1));
        return getNonPlayerProfile(texture);
    }

    private static GameProfile getNonPlayerProfile(String skinURL) {
        GameProfile newSkinProfile = new GameProfile(UUID.randomUUID(), null);
        newSkinProfile.getProperties().put("textures", new Property("textures",skinURL));
        return newSkinProfile;
    }

}
