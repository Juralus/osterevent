package de.pattyxdhd.osterevent.utils.mysql;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.pattyxdhd.osterevent.OsterEvent;
import de.pattyxdhd.osterevent.utils.objects.EasterPlayer;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DatabaseHandler {

    @Getter
    private MySQL mySQL;

    @Getter
    private Map<String, EasterPlayer> players = Maps.newHashMap();

    public DatabaseHandler(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public void loadPlayers(){
        try {
            players.clear();
            final ResultSet resultSet = getMySQL().preparedStatement("SELECT * FROM EasterPlayers").executeQuery();

            while (resultSet.next()){

                String[] foundEggsLocation = resultSet.getString("foundEggsLocation").split(":");
                List<Location> foundEggsLocationList = Lists.newArrayList();
                for (String s : foundEggsLocation) {
                    Location location = OsterEvent.getInstance().getConverter().convertStringToLocation(s);
                    if(location != null){
                        foundEggsLocationList.add(location);
                    }
                }

                final EasterPlayer easterPlayer = new EasterPlayer(resultSet.getString("uuid"), foundEggsLocationList);
                players.put(easterPlayer.getUuid(), easterPlayer);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean existsPlayer(final String uuid){
        try {
            final ResultSet resultSet = new PreparedStatementBuilder("SELECT * FROM EasterPlayers WHERE uuid=?", getMySQL())
                    .bindString(uuid)
                    .build().executeQuery();

            return resultSet.next();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public void insertPlayer(final Player player){
        getMySQL().update(new PreparedStatementBuilder("INSERT INTO EasterPlayers(uuid, foundEggsLocation) VALUES (?, ?)", getMySQL())
                .bindString(player.getUniqueId().toString())
                .bindString("")
                .build());
        players.put(player.getUniqueId().toString(), new EasterPlayer(player.getUniqueId().toString(), Lists.newArrayList()));
    }

    public void updatePlayer(final EasterPlayer easterPlayer){
        StringBuilder stringBuilder = new StringBuilder();
        easterPlayer.getFoundLocations().forEach(location -> {
            stringBuilder.append(OsterEvent.getInstance().getConverter().convertLocationToString(location)).append(":");
        });

        getMySQL().update(new PreparedStatementBuilder("UPDATE EasterPlayers SET foundEggsLocation=? WHERE uuid=?", getMySQL())
                .bindString(stringBuilder.toString())
                .bindString(easterPlayer.getUuid())
                .build());

        players.put(easterPlayer.getUuid(), easterPlayer);
    }

    public void deletePlayer(final String uuid){
        getMySQL().update(new PreparedStatementBuilder("DELETE FROM EasterPlayers WHERE uuid=?", getMySQL())
                .bindString(uuid)
                .build());
        getPlayers().remove(uuid);
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if(player != null){
            player.kickPlayer(OsterEvent.getInstance().getFileManager().getDeleteKickMessage().replace("%prefix%", OsterEvent.getInstance().getFileManager().getPrefix()));
        }
    }

}
